Tehnologii folosite:

    - Retrofit2: pentru apelurile HTTP.
    - SQLite Database: pentru stocarea informațiilor primite de la endpoint uri (country, name, id, whenToGo, isFavorite, longitude, latitude, windProbability).
    - Lombok: plugin pentru refactorizarea claselor Entity.
    - DTO from JSON: plugin pentru crearea claselor Entity.
 

Structura proiectului:

    Proiectul contine mai multe foldere, fiecare avand un scop specific.
    
    1. activity:  contine Activitati - MainActivity (ecranul principal - LIST), DetailsActivity (ecranul DETAILS), FilterActivity (ecranul FILTER).
    
    2. database: contine o clasa cu baza de date folosita.
    
    3. entity: contine clasele POJO necesare pentru apelurile HTTP; ele indica structura unui JSON intors.
    
    4: service: contine interfete, fiecare avand declaratia apelurilor HTTP ce se fac in cadrul unui end-point.
    
    5. endpoint: contine clase cu implementarea propriu-zisa pentru un end-point (apel + salvare informatii intoarse etc).
    
    6. adapter: contine o clasa cu un Custom Adapter folosit pentru afisarea informatiilor din ecranul LIST.
    
    7. util: contine clase ajutatoare
    

Notes:

    - Folosind Retrofit, codul poate fi extins, adica se pot adauga cu usurinta noi end-pointuri fara a crea dependinte intre clase, codul ramand 
    structurat.
    
    - Folosind un Custom Adapter la afisarea informatiilor din ecranul LIST, putem adauga cu usurinta ceva nou de afisat deoarece doar trebuie sa
    adaugam campul respectiv in clasa model pentru ListView.
    
    - Acelasi lucru este valabil si la Simple Adapter folosit la afisarea informatiilor din ecranul DETAILS. Orice informatie in plus care trebuie 
    afisata doar va fi adaugata in ArrayList in continuare.
    
    - Folosind o baza de date pentru a stoca informatiile, e simplu sa afli / modifici / adaugi / stochezi informatii.
    
    

Functionalitate:

Ecranul LIST: 

    - Pentru afisarea informatiilor din ecranul LIST se face un apel la /api-get-all deoarece astfel facem rost atat de numele unei tari, 
    cat si de spotul pentru kitesurfing si daca spotul este la Favorite sau nu.
    
    - Informatiile sunt stocate intr-o baza de date SQLite, iar pentru afisare se foloste un Custom Adapter care extinde ArrayAdapter si
    contine ca tip de obiecte clasa Spot. Un spot are campurile: String Country, String Name, Int id_image, exact ce avem nevoie pentru afisare.
    
    - Prima oara cand se descarca aplicatia pe telefon, informatiile se vor uploada mai greu, de aceea este nevoie de un refresh (inchidere si 
    redeschidere a aplicatiei). A doua oara informatiile, fiind deja stocate in baza de date, vor fi afisate direct din momentul deschiderii
    aplicatiei.
    
    - Apasarea pe numele unei tarii va deschide aplicatia DETAILS.
    
     - Apasarea pe iconita filtru va deschide aplicatia FILTER.
    
    - Apasarea pe iconita stea fie va adauga spotul la Favorite, fie il va sterge.
    
    - Se poate da si swipe down to refresh.


Ecranul DETAILS:

    - Inainte de a deschide aceasta activitate se face un apel la /api-get-details si se updateaza campurile latitude, longitude si windProbability
      din baza de date. Prima oara cand o tara este cautata de utilizator, updatarile nu vor fi vizibile, de aceea este nevoie de un refresh (back button
      si vizualizare tara din nou).
     
    - Apasarea pe iconita stea fie va adauga spotul la Favorite, fie il va sterge.
    
    - Se poate da si swipe down to refresh.

    - Se foloseste tot un Adapter pentru afisarea informatiilor, de data asta nu unul Custom, ci SimpleAdapter.
    
Ecranul FILTER:

    - In campul Country se pot introduce doar caractere din alfabet (orice cifra, sau caracter precum '*' ',' '/' '('  etc nu va permite filtrarea). 
      De asemena, este case-sensitive (orice tara incepe cu litera mare).
    
    - In campul WindProbability se pot introduce doar valori pozitive, pana in 100, inclusiv.


    