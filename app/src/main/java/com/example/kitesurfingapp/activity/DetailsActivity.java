package com.example.kitesurfingapp.activity;

import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.kitesurfingapp.R;
import com.example.kitesurfingapp.adapter.MyListAdapter;
import com.example.kitesurfingapp.database.Database;
import com.example.kitesurfingapp.endpoint.DetailsEndpoint;
import com.example.kitesurfingapp.endpoint.ListEndpoint;

import java.util.*;

public class DetailsActivity extends AppCompatActivity {
    Database db = new Database(this);
    SwipeRefreshLayout pullToRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        /* Swipe down to refresh data after an event */
        pullToRefresh = findViewById(R.id.refresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ListEndpoint.adapter.notifyDataSetChanged();
                pullToRefresh.setRefreshing(false);
            }
        });

        /* Set the title of Action Bar*/
        Bundle bundle = getIntent().getExtras();
        String title = null;
        if (bundle != null) {
            title = bundle.getString("title");
            setTitle(title);
        }

        Map<String, String> details = new TreeMap<>();
        List<TreeMap<String, String>> listItems = new ArrayList<>();
        String id = null;

        /* Get country's id for database query */
        Cursor data = db.onSelect(Database.COLUMN_ZERO + ", " + Database.COLUMN_TWO, "Country", title);
        if (data.moveToFirst()) {
            id = data.getString(0);
        }

        /* Update information of spot with the above id
         * using a call to /api-get-details */
        DetailsEndpoint.fetchSpotDetails(db, id);

        /* Get details of country based on its id */
        data = db.onSelect(Database.COLUMN_TWO + ", " +
                Database.COLUMN_FIVE + ", " +
                Database.COLUMN_SIX + ", " +
                Database.COLUMN_SEVEN + ", " +
                Database.COLUMN_THREE, "Id", id);

        /* Set details of country in a Map */
        if (data.moveToFirst()) {
            for (int i = 0; i < data.getColumnCount(); i++) {
                details.put(data.getColumnName(i).replaceAll("_", " "), data.getString(i));
            }
        }

        /* Create new SimpleAdapter for ListView to display details */
        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), listItems, R.layout.details_list,
                new String[]{"first", "second"},
                new int[]{R.id.text1, R.id.text2});

        /* Fill List for Adapter with content from Map */
        for (Object o : details.entrySet()) {
            TreeMap<String, String> map = new TreeMap<>();
            Map.Entry pair = (Map.Entry) o;
            map.put("first", pair.getKey().toString());
            map.put("second", pair.getValue().toString());
            listItems.add(map);
        }

        final ListView listView = findViewById(R.id.details_list_view);
        listView.setAdapter(adapter);
        db.close();

        /* First time the screen needs to be refreshed to update data */
        if (listItems.get(1).get("second").equals("0") &&
                listItems.get(2).get("second").equals("0") &&
                listItems.get(4).get("second").equals("0"))
            Toast.makeText(this, "Enter screen again to refresh.", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* Inflate the menu; this adds items to the action bar if it is present */
        getMenuInflater().inflate(R.menu.menu_details, menu);

        /* Set color to menu icons */
        Drawable drawable = menu.findItem(R.id.star).getIcon();
        drawable.setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);
        menu.findItem(R.id.star).setIcon(drawable);

        /* Set image to star menu icon */
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.getInt("button") == R.drawable.star_off)
                menu.getItem(0).setIcon(R.drawable.star_off);
            else
                menu.getItem(0).setIcon(R.drawable.star_on);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /* Handle action bar item clicks */
        switch (item.getItemId()) {
            case R.id.star:
                /* Get type of image and its row position
                   and change the image accordingly */
                Bundle bundle = getIntent().getExtras();
                if (bundle != null) {
                    int position = bundle.getInt("position");
                    int resource = MyListAdapter.spots.get(position).getImage();

                    if (resource == R.drawable.star_off) {
                        MyListAdapter.spots.get(position).setImage(R.drawable.star_on);
                        item.setIcon(R.drawable.star_on);
                        Log.i("Details Activity: ", "spot is now added to favorite.");
                    } else {
                        MyListAdapter.spots.get(position).setImage(R.drawable.star_off);
                        item.setIcon(R.drawable.star_off);
                        Log.i("Details Activity: ", "spot is now removed from favorite.");
                    }
                }
                Toast.makeText(this, "Changes made." + "\n" + "Swipe down in MainActivity to refresh.", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
