package com.example.kitesurfingapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kitesurfingapp.R;
import com.example.kitesurfingapp.database.Database;

public class FilterActivity extends AppCompatActivity {
    EditText countryText;
    EditText windText;

    Database db;
    public static Integer flag = 0;
    public static String countryInput, windInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        countryText = findViewById(R.id.CountryText);
        windText = findViewById(R.id.WindText);
        db = new Database(this);
    }

    /**
     * This method is triggered when APPLY button is clicked.
     * We validate for input and then set a flag to a value which
     * indicates what optional parameters of /api-get-all call we should pass.
     */
    public void apply(View view) {
        countryInput = countryText.getText().toString();
        windInput = windText.getText().toString();

        /* Validate input */
        if (!validateCountryInput()) return;
        if (!validateWindInput()) return;

        /* Set a value to flag */
        setTypeOfEndpointCall();

        Toast.makeText(this, "Search done! Check Kitesurfing App screen.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Check if Country input contains only characters
     * otherwise display an error message to user.
     */
    public boolean validateCountryInput() {
        if (!countryInput.isEmpty()) {
            String check = countryInput.replaceAll("[*a-zA-Z]", "");
            if (!check.isEmpty()) {
                countryText.setError("String must contain only characters.");
                return false;
            }
        }
        return true;
    }

    /**
     * Check if Wind input contains a value at most 100
     * otherwise display an error message to user.
     * Checking for input to contain only digit is done in .xml file
     * using android:inputType = number.
     */
    public boolean validateWindInput() {
        if (windInput.length() != 0) {
            if (Integer.parseInt(windInput) > 100) {
                windText.setError("Value must be at most 100.");
                return false;
            }
        }
        return true;
    }

    /**
     * Set a value to flag.
     * flag = 1: we have only "windProbability"
     * flag = 2: we have only "country"
     * flag = 3: we have "windProbability" and "country"
     * flag = 4: we don't have any optional parameters.
     * <p>
     * The value of the flag is checked in MainActivity from where
     * the call to /api-get-all is made.
     */
    public void setTypeOfEndpointCall() {
        if (!TextUtils.isEmpty(windInput) && TextUtils.isEmpty(countryInput)) {
            flag = 1;
        } else if (TextUtils.isEmpty(windInput) && !TextUtils.isEmpty(countryInput)) {
            flag = 2;
        } else if (!TextUtils.isEmpty(windInput) && !TextUtils.isEmpty(countryInput)) {
            flag = 3;
        } else {
            flag = 4;
        }
    }
}
