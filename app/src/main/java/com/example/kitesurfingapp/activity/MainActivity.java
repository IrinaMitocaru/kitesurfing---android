package com.example.kitesurfingapp.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.kitesurfingapp.R;
import com.example.kitesurfingapp.adapter.MyListAdapter;
import com.example.kitesurfingapp.database.Database;
import com.example.kitesurfingapp.endpoint.ListEndpoint;
import com.example.kitesurfingapp.endpoint.UserEndpoint;
import com.example.kitesurfingapp.util.Token;

public class MainActivity extends AppCompatActivity {

    static MainActivity activity;
    static Context context;
    static Database db;
    SwipeRefreshLayout pullToRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = new MainActivity();
        context = this.getApplicationContext();
        db = new Database(this);

        Token.sharedPref = getSharedPreferences("", Context.MODE_PRIVATE);
        Token.editor = Token.sharedPref.edit();

        /* Make a call to /api-get-user to get access token
         * and to /api-spot-get-all to retrieve data to be displayed */
        if (ListEndpoint.adapter == null) {
            Toast.makeText(this, "Data is loading..." + "\n" + "Enter application again if nothing appears.", Toast.LENGTH_LONG).show();
            fetchUser();
            fetchSpotList(db);
        }

        /* Swipe down to refresh data after an event */
        pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (ListEndpoint.adapter != null) {
                    ListEndpoint.adapter.notifyDataSetChanged();
                    pullToRefresh.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* Inflate the menu; this adds items to the action bar if it is present */
        getMenuInflater().inflate(R.menu.menu_main, menu);

        /* Set color to menu icons */
        Drawable drawable = menu.findItem(R.id.filterButton).getIcon();
        drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        menu.findItem(R.id.filterButton).setIcon(drawable);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /* Handle action bar item clicks */
        switch (item.getItemId()) {
            case R.id.filterButton:
                /* Create new Filter Activity */
                startActivity(new Intent(MainActivity.this, FilterActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Based on the value flag set on FilterActivy after user
     * provided an input, make a call to /api-get-all with some or
     * none optional parameters.
     * flag = 1: we have only "windProbability"
     * flag = 2: we have only "country"
     * flag = 3: we have "windProbability" and "country"
     * flag = 4: we don't have any optional parameters.
     */
    @Override
    public void onResume() {
        super.onResume();
        switch (FilterActivity.flag) {
            case 1:
                ListEndpoint.fetchSpotList(db, this, MainActivity.this, null, FilterActivity.windInput);
                break;
            case 2:
                ListEndpoint.fetchSpotList(db, this, MainActivity.this, FilterActivity.countryInput, null);
                break;
            case 3:
                ListEndpoint.fetchSpotList(db, this, MainActivity.this, FilterActivity.countryInput, FilterActivity.windInput);
                break;
            case 4:
                ListEndpoint.fetchSpotList(db, this, MainActivity.this, null, null);
                break;
        }
        FilterActivity.flag = 0;
    }

    /**
     * Method used to make a call to /api-get-user
     */
    private void fetchUser() {
        UserEndpoint.fetchUser();
    }

    /**
     * Method used to make a call to /api-get-all
     */
    private void fetchSpotList(Database db) {
        ListEndpoint.fetchSpotList(db, this, MainActivity.this, null, null);
    }

    /**
     * Method used to insert data into database
     */
    public void insertData(String id, String name, String country, String whenToGo,
                           String favourite, Double longitude, Double latitude, Integer wind) {
        db.onInsert(id, name, country, whenToGo, favourite, longitude, latitude, wind);
    }
}
