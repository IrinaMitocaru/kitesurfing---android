package com.example.kitesurfingapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kitesurfingapp.R;
import com.example.kitesurfingapp.database.Database;
import com.example.kitesurfingapp.activity.DetailsActivity;
import com.example.kitesurfingapp.endpoint.AddFavoriteEndpoint;
import com.example.kitesurfingapp.endpoint.RemoveFavoriteEndpoint;
import com.example.kitesurfingapp.util.Spot;

import java.util.ArrayList;

/**
 * Custom Adapter used to implement the view of a ListView.
 * Each row is modeled by Spot class.
 */
public class MyListAdapter extends ArrayAdapter<Spot> {

    private Database db;
    private Context context;
    private int resource;
    public static ArrayList<Spot> spots;

    public MyListAdapter(Database db, Context context, int resource, ArrayList<Spot> spots) {
        super(context, resource, spots);
        this.db = db;
        this.context = context;
        this.resource = resource;
        MyListAdapter.spots = spots;
    }

    @Override
    public @NonNull
    View getView(final int position, View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(resource, null);
        }

        /* Get row model items for current position */
        final Spot spot = spots.get(position);
        final ImageButton button = convertView.findViewById(R.id.imageButton);
        final TextView item = convertView.findViewById(R.id.text1);
        TextView subItem = convertView.findViewById(R.id.text2);

        /* Set fields from ListView to be displayed */
        button.setImageDrawable(context.getResources().getDrawable(spot.getImage()));
        button.setTag(spot.getImage());
        item.setText(spot.getItem());
        subItem.setText(spot.getSubItem());

        /*  CountryText onClick event */
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /* Create new Details Activity */
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("title", spot.getItem());
                intent.putExtra("button", spot.getImage());
                intent.putExtra("position", position);

                context.startActivity(intent);
            }
        });

        /*  Image icon onClick event */
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /* Set image as favorite and make a call to /api-add-favorites */
                if (button.getTag().equals(R.drawable.star_off)) {
                    button.setImageResource(R.drawable.star_on);
                    button.setTag(R.drawable.star_on);
                    spots.get(position).setImage(R.drawable.star_on);
                    fetchAddFavorite(db, spot.getItem());
                } else {
                    /* Remove image as favorite and make a call to /api-remove-favorites */
                    button.setImageResource(R.drawable.star_off);
                    button.setTag(R.drawable.star_off);
                    spots.get(position).setImage(R.drawable.star_off);
                    fetchRemoveFavorite(db, spot.getItem());
                }

            }
        });
        return convertView;
    }

    /**
     * The method is used to make a call to /api-add-favorites
     */
    private void fetchAddFavorite(Database db, String item) {
        AddFavoriteEndpoint.fetchAddFavorite(db, item);
    }

    /**
     * The method is used to make a call to /api-remove-favorites
     */
    private void fetchRemoveFavorite(Database db, String item) {
        RemoveFavoriteEndpoint.fetchRemoveFavorite(db, item);
    }
}
