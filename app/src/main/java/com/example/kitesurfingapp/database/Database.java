package com.example.kitesurfingapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {
    private static final String TABLE_NAME = "LIST";
    public static final String COLUMN_ZERO = "Id";
    public static final String COLUMN_ONE = "Name";
    public static final String COLUMN_TWO = "Country";
    public static final String COLUMN_THREE = "When_to_Go";
    public static final String COLUMN_FOUR = "Is_Favorite";
    public static final String COLUMN_FIVE = "Longitude";
    public static final String COLUMN_SIX = "Latitude";
    public static final String COLUMN_SEVEN = "Wind_Probability";

    private final static String SQL_CREATE_ENTRIES = "CREATE TABLE "
            + TABLE_NAME + "( "
            + COLUMN_ZERO + " TEXT,"
            + COLUMN_ONE + " TEXT,"
            + COLUMN_TWO + " TEXT,"
            + COLUMN_THREE + " TEXT,"
            + COLUMN_FOUR + " TEXT,"
            + COLUMN_FIVE + " NUMERIC,"
            + COLUMN_SIX + " NUMERIC,"
            + COLUMN_SEVEN + " INTEGER)";

    private final static String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
            + TABLE_NAME;

    public Database(Context context) {
        super(context, TABLE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onInsert(String id, String name, String country, String whenToGo,
                         String favourite, Double longitude, Double latitude, Integer wind) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ZERO, id);
        values.put(COLUMN_ONE, name);
        values.put(COLUMN_TWO, country);
        values.put(COLUMN_THREE, whenToGo);
        values.put(COLUMN_FOUR, favourite);
        values.put(COLUMN_FIVE, longitude);
        values.put(COLUMN_SIX, latitude);
        values.put(COLUMN_SEVEN, wind);

        db.insert(TABLE_NAME, null, values);
    }

    public void onUpdate(String id, String column, String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        switch (column) {
            case COLUMN_FOUR:
                values.put(column, value);
                break;
            case COLUMN_FIVE:
                values.put(column, Double.parseDouble(value));
                break;
            case COLUMN_SIX:
                values.put(column, Double.parseDouble(value));
                break;
            case COLUMN_SEVEN:
                values.put(column, Integer.parseInt(value));
                break;
        }
        db.update(TABLE_NAME, values, "Id" + "= '" + id + "'", null);
    }

    public Cursor onSelect(String column, String atribute, String condition) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data;
        if (column != null) {
            /* SELECT (...) FROM TABLE */
            if (atribute == null && condition == null) {
                data = db.rawQuery("SELECT " + column + " FROM " + TABLE_NAME, null);
            } else {
                /* SELECT (...) FROM TABLE WHERE (...) */
                data = db.rawQuery("SELECT " + column + " FROM " + TABLE_NAME + " WHERE " + atribute + " = '" + condition + "'", null);
            }
        } else {
            /* SELECT * FROM TABLE WHERE (...) */
            if (atribute != null && condition != null) {
                data = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + atribute + " = '" + condition + "'", null);
            } else {
                /* SELECT * FROM TABLE */
                data = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            }
        }
        return data;
    }
}
