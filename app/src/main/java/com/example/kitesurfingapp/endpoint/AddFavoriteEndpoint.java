package com.example.kitesurfingapp.endpoint;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.kitesurfingapp.database.Database;
import com.example.kitesurfingapp.entity.AddFavorite;
import com.example.kitesurfingapp.service.AddFavoriteService;
import com.example.kitesurfingapp.util.RetrofitClient;
import com.example.kitesurfingapp.util.Token;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AddFavoriteEndpoint {

    /**
     * Make a HTTP call to /api-spot-favorites-add with a token and spotId,
     * and change the icon properly on success.
     */
    public static void fetchAddFavorite(Database db, final String country) {
        String id = null;

        /* Get country's id with a database query */
        Cursor data = db.onSelect(Database.COLUMN_ZERO + ", " + Database.COLUMN_TWO, "Country", country);
        if (data.moveToFirst()) {
            id = data.getString(0);
        }

        /* Get Retrofit instance */
        Retrofit retrofit = RetrofitClient.getRetrofitClient();

        /* Create HTTP call */
        AddFavoriteService service = retrofit.create(AddFavoriteService.class);

        HashMap<String, String> spotId = new HashMap<>();
        spotId.put("spotId", id);

        HashMap<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", Token.sharedPref.getString("token", ""));

        /* Invoke method corresponding to the HTTP request */
        Call<AddFavorite> call = service.postAddFavorite(tokenMap, spotId);

        /* Send a network request */
        call.enqueue(new Callback<AddFavorite>() {
            @Override
            public void onResponse(@NonNull Call<AddFavorite> call, @NonNull Response<AddFavorite> response) {
                /* Successful callback */
                if (response.body() != null) {
                    /* Set new icon */
                    Log.i("On response -> ", "/api-favorites-add " + country + " spot added from favorite");
                }
            }

            @Override
            public void onFailure(@NonNull Call<AddFavorite> call, @NonNull Throwable t) {
                /* Failed callback */
                Log.e("On failure -> ", "/api-favorites-add" + t.getLocalizedMessage());
            }
        });
    }
}
