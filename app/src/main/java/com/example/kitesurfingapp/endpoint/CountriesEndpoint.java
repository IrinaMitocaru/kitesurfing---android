package com.example.kitesurfingapp.endpoint;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.kitesurfingapp.R;
import com.example.kitesurfingapp.activity.MainActivity;
import com.example.kitesurfingapp.database.Database;
import com.example.kitesurfingapp.entity.SpotCountries;
import com.example.kitesurfingapp.service.SpotCountriesService;
import com.example.kitesurfingapp.util.RetrofitClient;
import com.example.kitesurfingapp.util.Token;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CountriesEndpoint {

    /**
     * Make a HTTP call to /api-spot-get-countries with a token,
     * store get the list of countries.
     */
    public static void fetchSpotCountries(final Database db, final MainActivity activity) {
        /* Get Retrofit instance */
        Retrofit retrofit = RetrofitClient.getRetrofitClient();

        /* Create HTTP call */
        SpotCountriesService service = retrofit.create(SpotCountriesService.class);

        HashMap<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", Token.sharedPref.getString("token", ""));

        /* Invoke method corresponding to the HTTP request */
        Call<SpotCountries> call = service.postSpotCountries(tokenMap);

        /* Send a network request */
        call.enqueue(new Callback<SpotCountries>() {
            @Override
            public void onResponse(@NonNull Call<SpotCountries> call, @NonNull Response<SpotCountries> response) {
                /* Successful callback */
                if (response.body() != null) {
                    SpotCountries responseText = response.body();

                    for (String item : responseText.getResult())
                        activity.insertData(null, null, item, "", "", 0.0, 0.0, 0);

                    Log.i("onResponse -> ", "/api-get-countries");

                    ListView listView = activity.findViewById(R.id.list_view);
                    ArrayList<String> countryList = new ArrayList<>();
                    Cursor data = db.onSelect("Country", null, null);
                    if (data.getCount() != 0) {
                        while (data.moveToNext()) {
                            countryList.add(data.getString(2));
                            ListAdapter listAdapter = new ArrayAdapter<>(activity.getApplicationContext(), android.R.layout.simple_list_item_1, countryList);
                            listView.setAdapter(listAdapter);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SpotCountries> call, @NonNull Throwable t) {
                /* Failed callback */
                Log.e("onFailure -> ", "/api-get-countries " + t.getLocalizedMessage());
            }
        });
    }
}
