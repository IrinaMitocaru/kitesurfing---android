package com.example.kitesurfingapp.endpoint;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.kitesurfingapp.database.Database;
import com.example.kitesurfingapp.entity.SpotDetails;
import com.example.kitesurfingapp.service.SpotDetailsService;
import com.example.kitesurfingapp.util.RetrofitClient;
import com.example.kitesurfingapp.util.Token;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetailsEndpoint {

    /**
     * Make a HTTP call to /api-spot-get-details with a token,
     * and update the SQLite database with the received data.
     */
    public static void fetchSpotDetails(final Database db, final String id) {

        /* Get Retrofit instance */
        Retrofit retrofit = RetrofitClient.getRetrofitClient();

        /* Create HTTP calls from SpotDetailsService Interface */
        SpotDetailsService spotDetailsService = retrofit.create(SpotDetailsService.class);

        HashMap<String, String> spotId = new HashMap<>();
        spotId.put("spotId", id);

        HashMap<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", Token.sharedPref.getString("token", ""));

        /* Invoke method corresponding to the HTTP request */
        Call<SpotDetails> call = spotDetailsService.postSpotDetails(tokenMap, spotId);

        /* Send a network request */
        call.enqueue(new Callback<SpotDetails>() {
            @Override
            public void onResponse(@NonNull Call<SpotDetails> call, @NonNull Response<SpotDetails> response) {
                /* Successful callback */
                if (response.body() != null) {
                    /* Update longitude, latitude, wind, is favorite for given id */
                    SpotDetails.Result responseText = response.body().getResult();

                    db.onUpdate(id, Database.COLUMN_FOUR, responseText.getIsFavorite());
                    db.onUpdate(id, Database.COLUMN_FIVE, responseText.getLongitude().toString());
                    db.onUpdate(id, Database.COLUMN_SIX, responseText.getLatitude().toString());
                    db.onUpdate(id, Database.COLUMN_SEVEN, responseText.getWindProbability().toString());

                    Log.i("onResponse -> ", "/api-spot-get-details");
                }
            }

            @Override
            public void onFailure(@NonNull Call<SpotDetails> call, @NonNull Throwable t) {
                /* Error callback */
                Log.e("onFailure -> ", "/api-spot-get-details " + t.getLocalizedMessage());
            }
        });
    }
}
