package com.example.kitesurfingapp.endpoint;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ListView;

import com.example.kitesurfingapp.R;
import com.example.kitesurfingapp.adapter.MyListAdapter;
import com.example.kitesurfingapp.database.Database;
import com.example.kitesurfingapp.entity.SpotList;
import com.example.kitesurfingapp.service.SpotListService;
import com.example.kitesurfingapp.activity.MainActivity;
import com.example.kitesurfingapp.util.RetrofitClient;
import com.example.kitesurfingapp.util.Spot;
import com.example.kitesurfingapp.util.Token;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ListEndpoint {
    public static MyListAdapter adapter;

    /**
     * Make a HTTP call to /api-spot-get-all with a token,
     * store the received data into a SQLite database
     * and create a Custom Adapter to display data.
     */
    public static void fetchSpotList(final Database db, final MainActivity activity, final Context context, final String countryName, final String wind) {
        /* Get Retrofit instance */
        Retrofit retrofit = RetrofitClient.getRetrofitClient();

        /* Create HTTP call */
        SpotListService service = retrofit.create(SpotListService.class);

        HashMap<String, String> tokenMap = new HashMap<>();
        HashMap<String, Object> body = new HashMap<>();

        tokenMap.put("token", Token.sharedPref.getString("token", ""));

        Call<SpotList> call;
        Integer windValue = 0;
        if (wind != null)
            windValue = Integer.parseInt(wind);

        /* Invoke method corresponding to the HTTP request
         * with some or none optional parameters */
        if (countryName == null && wind == null) {
            call = service.postSpotList(tokenMap, body);
        } else if (countryName != null && wind == null) {
            body.put("country", countryName);
            call = service.postSpotList(tokenMap, body);
        } else if (countryName == null) {
            body.put("windProbability", windValue);
            call = service.postSpotList(tokenMap, body);
        } else {
            body.put("country", countryName);
            body.put("windProbability", windValue);
            call = service.postSpotList(tokenMap, body);
        }

        /* Send a network request */
        call.enqueue(new Callback<SpotList>() {
            @Override
            public void onResponse(@NonNull Call<SpotList> call, @NonNull Response<SpotList> response) {
                /* Successful callback */
                if (response.body() != null) {
                    SpotList responseText = response.body();
                    HashSet<SpotList.Result> result = responseText.getResult();

                    int resource;
                    String country, spotName;
                    ArrayList<Spot> spots = new ArrayList<>();

                    for (SpotList.Result index : result) {
                        /* We have optional parameters, don't insert data into database,
                         * just store data into an ArrayList  to be displayed */
                        if (countryName != null || wind != null) {
                            country = index.getCountry();
                            spotName = index.getName();
                            resource = (index.getIsFavorite().equals("false")) ?
                                    R.drawable.star_off :
                                    R.drawable.star_on;
                            spots.add(new Spot(resource, country, spotName));
                        } else {
                            /* We don't have optional parameters.
                             * Insert data into SQLite database. */
                            activity.insertData(index.getId(),
                                    index.getName(),
                                    index.getCountry(),
                                    index.getWhenToGo(),
                                    index.getIsFavorite(), 0.0, 0.0, 0);
                        }
                    }

                    /* Make a query to select Country, Spot name and is_Favorite from database
                     * if no parameters where given */
                    Cursor data;
                    if (countryName == null && wind == null) {
                        data = db.onSelect(Database.COLUMN_ONE +
                                ", " + Database.COLUMN_TWO +
                                ", " + Database.COLUMN_FOUR, null, null);

                        /* Store data into an ArrayList for Custom Adapter */
                        if (data.moveToFirst()) {
                            do {
                                if (data.getString(0) != null && data.getString(1) != null) {
                                    resource = (data.getString(2).equals("false")) ?
                                            R.drawable.star_off :
                                            R.drawable.star_on;
                                    country = data.getString(1);
                                    spotName = data.getString(0);
                                    spots.add(new Spot(resource, country, spotName));
                                }
                            } while (data.moveToNext());
                        }
                        data.close();
                    }

                    /* Create new Custom Adapter  */
                    adapter = new MyListAdapter(db, context, R.layout.list_view_items, spots);

                    ListView listView = activity.findViewById(R.id.list_view);
                    listView.setAdapter(adapter);

                    Log.i("onResponse -> ", "/api-get-all");
                }
            }

            @Override
            public void onFailure(@NonNull Call<SpotList> call, @NonNull Throwable t) {
                /* Failed callback */
                Log.e("onFailure -> ", "/api-get-all " + t.getLocalizedMessage());
            }
        });
        db.close();
    }
}
