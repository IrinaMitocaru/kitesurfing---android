package com.example.kitesurfingapp.endpoint;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.kitesurfingapp.entity.User;
import com.example.kitesurfingapp.service.UserService;
import com.example.kitesurfingapp.util.RetrofitClient;
import com.example.kitesurfingapp.util.Token;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UserEndpoint {

    /**
     * Make a HTTP call to /api-user-get with a valid mail
     * and store the received token in a SharedPreferences instance.
     */
    public static void fetchUser() {
        /* Get Retrofit instance */
        Retrofit retrofit = RetrofitClient.getRetrofitClient();

        /* Create HTTP call */
        UserService service = retrofit.create(UserService.class);

        HashMap<String, String> email = new HashMap<>();
        email.put("email", "mitocaruirina@yahoo.com");

        /* Invoke method corresponding to the HTTP request */
        Call<User> call = service.postUser(email);

        /* Send a network request */
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                /* Successful callback */
                if (response.body() != null) {
                    /* Retrieve token */
                    User responseText = response.body();
                    String token = responseText.getResult().getToken();

                    Token.editor
                            .putString("token", token)
                            .apply();

                    Log.i("onResponse -> ", "/api-user-get");
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                /* Failed callback */
                Log.e("onFailure -> ", "/api-user-get " + t.toString());
            }
        });
    }
}
