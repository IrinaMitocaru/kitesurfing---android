package com.example.kitesurfingapp.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class AddFavorite {
    @Expose
    @SerializedName("sentHeaders")
    private SentHeaders sentHeaders;
    @Expose
    @SerializedName("sentBodyParameters")
    private SentBodyParameters sentBodyParameters;
    @Expose
    @SerializedName("error")
    private Error error;
    @Expose
    @SerializedName("result")
    private String result;

    @Getter
    @Setter
    private static class SentHeaders {
        @Expose
        @SerializedName("token")
        private String token;
        @Expose
        @SerializedName("content-type")
        private String contentType;
    }

    @Getter
    @Setter
    private static class SentBodyParameters {
        @Expose
        @SerializedName("spotId")
        private String spotId;
    }

    @Getter
    @Setter
    private static class Error {
        @Expose
        @SerializedName("code")
        private String code;
        @Expose
        @SerializedName("message")
        private String message;
    }
}
