package com.example.kitesurfingapp.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpotCountries {
    @Expose
    @SerializedName("result")
    private List<String> result;
}
