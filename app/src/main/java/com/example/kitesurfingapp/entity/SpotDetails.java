package com.example.kitesurfingapp.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SpotDetails {
    @Expose
    @SerializedName("result")
    private Result result;

    @Getter
    @Setter
    public static class Result {
        @Expose
        @SerializedName("isFavorite")
        private String isFavorite;
        @Expose
        @SerializedName("whenToGo")
        private String whenToGo;
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("windProbability")
        private Integer windProbability;
        @Expose
        @SerializedName("latitude")
        private Double latitude;
        @Expose
        @SerializedName("longitude")
        private Double longitude;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;
    }
}
