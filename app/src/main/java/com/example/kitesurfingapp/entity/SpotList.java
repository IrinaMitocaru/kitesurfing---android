package com.example.kitesurfingapp.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashSet;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SpotList {
    @Expose
    @SerializedName("result")
    private HashSet<Result> result;

    @Getter
    @Setter
    @ToString
    @EqualsAndHashCode
    public static class Result {
        @Expose
        @SerializedName("country")
        private String country;
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("id")
        private String id;
        @Expose
        @SerializedName("whenToGo")
        private String whenToGo;
        @Expose
        @SerializedName("isFavorite")
        private String isFavorite;
    }
}
