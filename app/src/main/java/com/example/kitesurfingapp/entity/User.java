package com.example.kitesurfingapp.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class User {
    @Expose
    @SerializedName("result")
    private Result result;

    @Getter
    @Setter
    public static class Result {
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("token")
        private String token;
    }
}
