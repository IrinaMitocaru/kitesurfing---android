package com.example.kitesurfingapp.service;

import com.example.kitesurfingapp.entity.AddFavorite;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface AddFavoriteService {

    @POST("/api-spot-favorites-add")
    Call<AddFavorite> postAddFavorite(@HeaderMap Map<String, String> token, @Body HashMap<String, String> spotId);
}
