package com.example.kitesurfingapp.service;

import com.example.kitesurfingapp.entity.RemoveFavorite;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface RemoveFavoriteService {

    @POST("/api-spot-favorites-remove")
    Call<RemoveFavorite> postRemoveFavorite(@HeaderMap Map<String, String> token, @Body HashMap<String, String> spotId);
}
