package com.example.kitesurfingapp.service;

import com.example.kitesurfingapp.entity.SpotCountries;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface SpotCountriesService {

    @POST("/api-spot-get-countries")
    Call<SpotCountries> postSpotCountries(@HeaderMap Map<String, String> token);
}
