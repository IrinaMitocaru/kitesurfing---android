package com.example.kitesurfingapp.service;

import com.example.kitesurfingapp.entity.SpotDetails;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface SpotDetailsService {

    @POST("/api-spot-get-details")
    Call<SpotDetails> postSpotDetails(@HeaderMap Map<String, String> token, @Body HashMap<String, String> spotId);
}
