package com.example.kitesurfingapp.service;

import com.example.kitesurfingapp.entity.SpotList;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface SpotListService {
    @POST("/api-spot-get-all")
    Call<SpotList> postSpotList(@HeaderMap Map<String, String> token, @Body Map<String, Object> optionalParam);
}
