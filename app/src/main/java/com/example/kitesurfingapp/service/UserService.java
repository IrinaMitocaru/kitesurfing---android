package com.example.kitesurfingapp.service;

import com.example.kitesurfingapp.entity.User;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {

    @POST("/api-user-get")
    Call<User> postUser(@Body HashMap<String, String> email);
}
