package com.example.kitesurfingapp.util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Return a Retrofit client instance.
 * This method uses Singleton Pattern and Lazy Initialization.
 */
public class RetrofitClient {
    private static final String BASE_URL = "https://internship-2019.herokuapp.com";
    private static Retrofit retrofit;

    public static Retrofit getRetrofitClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
