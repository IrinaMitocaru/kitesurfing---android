package com.example.kitesurfingapp.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class is used to model one row of a ListView.
 */
@Getter
@Setter
@AllArgsConstructor
public class Spot {

    int image;
    String item;
    String subItem;

}
